modprobe ath9k
modprobe ath5k # just in case
modprobe batman-adv
modprobe bridge

sleep 5

ifconfig wlan0 up
ifconfig wlan0 0.0.0.0 down
ifconfig wlan0 mtu 1532
iwconfig wlan0 mode ad-hoc essid batman-mesh ap 43:5F:6B:88:A1:CB channel 11

sleep 5
iwconfig wlan0

brctl addbr mesh0
brctl addif mesh0 wlan0
batctl if add mesh0

sysctl -w net.ipv4.ip_forward=1


# Set up IP address from last octets of control interface
x=$(ifconfig | awk '/inet addr/{print substr($2,6)}' | head -n 1 | cut -f3 -d'.')
y=$(ifconfig | awk '/inet addr/{print substr($2,6)}' | head -n 1 | cut -f4 -d'.')

ifconfig mesh0 up
ifconfig wlan0 up
ifconfig bat0 192.168.$x.$y netmask 255.255.0.0

ifconfig bat0

