modprobe ath9k
modprobe ath5k # just in case

sleep 5

ifconfig wlan0 0.0.0.0 down

iw dev wlan0 interface add mesh0 type mp
iw dev mesh0 set channel 11

# Set up IP address from last octets of control interface                       
x=$(ifconfig | awk '/inet addr/{print substr($2,6)}' | head -n 1 | cut -f3 -d'.')
y=$(ifconfig | awk '/inet addr/{print substr($2,6)}' | head -n 1 | cut -f4 -d'.')

brctl addbr br0                                                               
brctl addif br0 mesh0

ifconfig br0 192.168.$x.$y netmask 255.255.0.0

ifconfig mesh0 up
iw dev mesh0 mesh join 80211s-mesh

sysctl -w net.ipv4.ip_forward=1

sleep 5 

iw dev mesh0 station dump

ifconfig br0
