This is a repository with scripts for the [mesh network](https://witestlab.poly.edu/blog/tag/mesh-network/)
experiments on the [Run My Experiment on GENI](https://witestlab.poly.edu/blog) blog.
