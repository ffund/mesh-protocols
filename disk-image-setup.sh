# Based on the other "setup" documents, a generic disk image for all wireless
# routing protocols involves the following (no-intervention) setup.
#
# If this repo is public, you can run this script on a node with:
# wget -O - https://bitbucket.org/ffund/wireless-mesh-experiment-docs/raw/master/disk-image-setup.sh | bash


########################################
#       Get prerequisites              #
########################################

apt-get update
apt-get -y install flex bison git cmake pkg-config libnl-genl-3-dev libtomcrypt-dev doxygen pkg-config libnl-genl-3-dev ebtables

########################################
#       Get software                   #
########################################

git clone https://github.com/OLSR/OONF.git

wget http://www.olsr.org/releases/0.9/olsrd-0.9.0.3.tar.gz
tar xzvf olsrd-0.9.0.3.tar.gz

wget https://downloads.open-mesh.org/batman/stable/sources/batman-adv/batman-adv-2017.0.tar.gz
tar -xzvf batman-adv-2017.0.tar.gz
wget https://downloads.open-mesh.org/batman/stable/sources/batctl/batctl-2017.0.tar.gz
tar -xzvf batctl-2017.0.tar.gz

wget https://www.irif.fr/~jch/software/files/babeld-1.8.0.tar.gz
tar -xzvf babeld-1.8.0.tar.gz

########################################
#       Install software               #
########################################

cd /root/OONF/build
cmake ..
make
make install

cd /root/olsrd-0.9.0.3
make
make install

cd /root/batman-adv-2017.0/
make
make install

cd /root/batctl-2017.0/
make
make install

cd /root/babeld-1.8.0
make
make install

########################################
#       Fix bridging                   #
########################################

cd
apt-get source linux-source-$(uname -r | cut -d'-' -f1)
apt-get install linux-headers-$(uname -r)

cd linux-$(uname -r | cut -d'-' -f1)
make oldconfig
make prepare
make scripts

cp -v /usr/src/linux-headers-$(uname -r)/Module.symvers .
mv -v /lib/modules/$(uname -r)/kernel/net/bridge/bridge.ko /lib/modules/$(uname -r)/kernel/net/bridge/bridge.ko.backup

cd net/bridge
cp br_if.c br_if.c.backup
# comment two lines starting with the one that has IFF_DONT_BRIDGE
sed -e '/IFF_DONT_BRIDGE/,+1 s.^.//.' br_if.c.backup > br_if.c

make -C /lib/modules/$(uname -r)/build M=$(pwd) modules
make -C /lib/modules/$(uname -r)/build M=$(pwd) modules_install

depmod

########################################
#       Clean up                       #
########################################

cd
rm *.gz

apt-get -y install bridge-utils