modprobe ath9k
modprobe ath5k # just in case

sleep 5

sysctl -w net.ipv4.ip_forward=1

ifconfig wlan0 up
ifconfig wlan0 0.0.0.0 down
iwconfig wlan0 mode ad-hoc essid babel-mesh ap 43:5F:6B:88:B1:CB channel 11

brctl addbr br0
brctl addif br0 wlan0

# Set up IP address from last octets of control interface                       
x=$(ifconfig | awk '/inet addr/{print substr($2,6)}' | head -n 1 | cut -f3 -d'.')
y=$(ifconfig | awk '/inet addr/{print substr($2,6)}' | head -n 1 | cut -f4 -d'.')
                                                                                
ifconfig wlan0 up                                                               
ifconfig br0 192.168.$x.$y netmask 255.255.0.0                                 

# On some testbeds it may be eth0...
babeld -g 33123 -D -d 1 -C 'redistribute local if eth1 deny' br0
